// rollup.config.js
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';

export default {
  input: './index.js',
  output: {
    file: './index.cjs',
    format: 'cjs'
  },
  plugins: [
    commonjs({
      transformMixedEsModules: true
    })
    ,nodeResolve({
      resolveOnly: ['base32-encode','to-data-view']
    })
  ]
};