import assert from 'node:assert'
import TwtHash from './index.js';

const twt = {
	// The extra escape is necessary
	content: "We're still here\\!",
	created: "2023-10-27T10:05:39+0200",
	url: "https://johanbove.info/twtxt.txt"
}

const theHash = TwtHash(twt);
const expected = "lifd5sq";

assert.equal(theHash, expected);