# TwtHash

This is a JavaScript implementation of TwtHash, a Twtxt extension.

## Example

A _twtxt_ message with the content "We're still here!" from the Twtxt file "https://johanbove.info/twtxt.txt" can be hashed like this: 

```bash
node main.js "We're still here\!" "2023-10-27T10:05:39+0200" "https://johanbove.info/twtxt.txt"
```

The script should then output:

```bash
The Twthash is lifd5sq
```

## Development

Make sure you have _Node 18_ installed. The easiest option is to install the _nvm_ node version manager.

Then you can get the right node version with the command:

		nvm use

When Node is installed, you can then continue developing and testing.

Install the dependencies:

		npm install

You can run the basic smoke-test with the command:

		npm run test

It is also possible to run the script with the bash command:

		npm run twthash "My Twt" $(date +"%Y-%m-%dT%H:%M:%S%z") "https://johanbove.info/twtxt.txt"

Note that the hash output will be always different since we're using the time at the moment the command is executed!

## Resources

- <https://github.com/nvm-sh/nvm>
- <https://dev.twtxt.net/>
- <https://dev.twtxt.net/doc/twthashextension.html>
- <https://twtxt.readthedocs.io/>


EOF